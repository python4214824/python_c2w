def outer(flag):
    def inner():
        return "this is true. "if flag else "this is false"
    return inner
if __name__=="__main__":
    true=outer(True)
    false=outer(False)
    print(true())
    print(false())
