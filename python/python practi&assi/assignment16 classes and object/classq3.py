class parent:
    def __init__(self):
        print("in constructor")
    @classmethod
    def clsmeth(cls):
        print("in classmethod")
    @staticmethod
    def statmethod():
        print("in static method")
class child(parent):
    pass
obj=child()
obj.clsmeth()
obj.statmethod()
