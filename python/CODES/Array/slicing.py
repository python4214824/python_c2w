import array as arr
b=arr.array("i",[1,2,3,4,5,6,7])
for i in b:
    print(i)
print("*************")
for j in b[1:4:]:
    print(j)
print("****************")
for i in b[1:4:1]:
    print(i)
print("*****************")
for i in b[4:1:-1]:
    print(i)
print("**********")
for i in b[4:6:1]:
    print(i)
print("****************")
print(b[3])
