#operator
#unary
print("-----unary-----")
x=10
y=20
print(x++y)             #x +y
print()


#binary
#arithematic
print("-----binary (arithematic)------")
print(x+y)              #addition
print(x-y)              #sub
print(x/y)              #div
print(x*y)              #mul
print(x%y)              #moulous
print(x//y)             #floor div
print(x**y)             #power
print()


#relational
print("-----relational----")
print(x<y)
print(x>y)
print(x<=y)
print(x>=y)
print(x==y)
print(x!=y)#not equal to
print()


#logical(and or not)
print("----------logical-------")
a=0
b=20
print(a and b)                          #20
print(a or b)                           #20
#print(a not b) will give error
print(not b)                            #False
print(not a)                            #True
print()


#assignment
print("--------assignment-----")
x+=y
print(x)
print()

#identity(is,is not)
print("-------identity------")

print(x is y)
print(x is not y)
print()


#membership (in,not in)
print("--------membership---------")
list=[1,2,3,4,5,10,"abc"  ,8]
print(2 in list)
print(x in list)
print(x not in list)
print()

#bitwise(or,and,xor,leftshift,right shift,negation)
print("---------bitwise---------")
p=10
q=5
print(p&5)
print(p^q)
print(10|q)
print(p>>2)             #00001010  ,00000010(2) right
print(p<<2)             #00101000(40) left
print(~p)               #2's complement 00001010
                                    #          1
                                    #   00001011
s=128
print(~128)


















                                




