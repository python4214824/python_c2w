class boss:
    def report(self):
        print("boss : rpwort")

class manager1(boss):
    def report(self):
        print("inproject:manager1")

class manager2(boss):
    def report(self):
        print("inproject:manager2")

class manager3(boss):
    def report(self):
        print("inproject:manager3")

class teamlead1(manager1,manager3):
      def report(self):
        print("in project:teamlead1")

class teamlead2(manager2,manager3):
    def report(self):
        print("in project:teamlead2")

class developer(teamlead1,teamlead2):
    def report(self):
        print("developer report")
print(developer.__mro__)#will give o/p in python2
print(developer.mro())#error in python 2
