class parent:
    x=10
    y=20
    @classmethod
    def dispdata(cls):
        print("in parent")
        print(cls.x)
        print(cls.y)
class child(parent):
    x=30
    y=40
    @classmethod
    def dispdata(cls):
        print("in child")
        print(cls.x)
        print(cls.y)
        super().dispdata()
obj=child()
obj.dispdata()
