class demo:
    def __init__(self):
        print("in constructor")
    def __del__(self):
        print("destructor")
def fun():
    print("in fun")
    obj=demo()
    print("end fun")
fun()
print("end code")
#when fun func finishes obj will be deleted
