#isprintable()checks whether all characyers in the string are printable
#/n,\t are not printable so it returns false
str1="hello,World"
str2="line\nline2"
str3="\t"
print(str1.isprintable())
print(str2.isprintable())
print(str3.isprintable())
