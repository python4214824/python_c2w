#split()used to split string into a alist of substring based on specified
#separator.if no separator is provided,it splits the string using whitespace by default
str1="core2web,incubator,binecaps"
print(str1.split())
print(str1.split(" "))
print(str1.split(","))
