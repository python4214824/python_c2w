s='akanksha'
print(s)
print(type(s))
print(len(s))
print()

x="incubator"
print(x)
print(type(x))
print(len(x))
print()

y='''binecaps
system
pvt
ltd'''
print(y)
print(type(y))
print(len(y))
print()

str="welcome to\tcore2web \npune"
print(str)
print(len(str))
print()
#/n,/t is considered as 1 while measuring length

#rawstring
str2=r"hii hello\takanksha"
print(str2)

str3="\u0906\u0908"
print(str3)
print(len(str3))
