#encode()
#the encode(encoding)method is used to encode the string using the specified encoding
#(default is'utf-8').
#the encoded string is represented as bytes,and b prefix indicates a bytes literal.
str1="core2web binecaps"
print(str1)
print(str1.encode('utf-8'))
