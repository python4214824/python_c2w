#isidentifier()checks if string is a valid python identifier(variable name)
#valid identifier means it must only conside alphnum values(a-z) and (0-9) or underscore
#a  valid identifier cannot start with a  number or contain any spaces
str1="akanksha09"
str2="akanksha phalke"
#str3=123 it has no attribute isidentifier
str4="09akanksha"
print(str1.isidentifier())
print(str2.isidentifier())
#print(str3.isidentifier())
print(str4.isidentifier())
