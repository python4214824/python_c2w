#casefold()returns version of string suitable for caseinsensitive comparision
str1="core2web Incubator"
str2="Core2web Incubator"
print(str1==str2)
print(str1.casefold()==str2.casefold())
print(str1==str2)
