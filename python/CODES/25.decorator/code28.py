def decfun(func):
    def wrapper():
        print("set wrapper")
        func()
        print("end wrapper")
    return wrapper
@decfun
def normfun():
    print("hello in normal fun")
normfun()

