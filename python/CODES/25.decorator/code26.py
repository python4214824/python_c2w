def decfun(func):
    def wrapper():
        print("set wrapper")
        func()
        print("end wrapper")
    return wrapper
def normfun():
    print("hello in normal fun")
normfun=decfun(normfun)
normfun()
