class parent:
    def __init__(self):
        print("in parent constructor")

    def parentfun(self):
        print("in parent func")
class child(parent):
    def __init__(self):
        super().__init__()
        print("in constructor child")
    def child(self):
        print("in child func")
obj1=child()
obj1.parentfun()
obj1.child()
