class parent:
    def __init__(self):
        print("in constructor")
    def parentfun(self):
        print("in parent func")
class child(parent):
    def __init__(self):
        print("child constructor")
    def childfun(self):
        print("in child fun")
obj1=child()
obj2=parent()
obj1.childfun()
obj1.parentfun()
