class parent:
    def __init__(self):
        print("in constructor")
    def parentfun(self):
        print("in parent fun")
class child(parent):
    def __init__(self):
        print("in child constructor")
        super().__init__(self)#self argument is not required so it will produce error
    def childfun(self):
        print("in child fun")
obj1=child()
obj1.parentfun()
obj1.childfun()
