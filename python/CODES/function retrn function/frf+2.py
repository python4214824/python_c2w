def outer(a,b):
    def inner(x,y):
        print("inner")
        return x+y
    print("outer")
    print(a*b)
    return inner
reobj=outer(2,3)
a=reobj(4,5)
print(a)
print(reobj)#each time address will be different
print(type(reobj))
print(type(a))
