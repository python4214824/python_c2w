class parent1:
    def __init__(self):
        print("in constructor parent 1")
    def dispdata(self):
        print('dispdata')
class parent2:
    def __init__(self):
        print("in constructor parent2")
    def printdata(self):
        print("in printdata")
class child(parent2,parent1):
    pass
obj=child()
obj.printdata()
obj.dispdata()
