#multiple inheritance
#a->b,c->b
class flyable:
    def fly(self):
        print("flying")
class swimmable:
    def swim(self):
        print("swimming")
class flyfish(flyable,swimmable):
    pass
myfish=flyfish()
myfish.fly()
myfish.swim()
