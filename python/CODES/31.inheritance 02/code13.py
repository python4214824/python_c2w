#a->b->cmultilevel inheritance
class animal:
    def eat(self):
        print("eating")
class mammal(animal):
    def breathe(self):
        print("breathing")
class dog(mammal):
    def bark(self):
        print("barking")
my_dog=dog()
my_dog.breathe()
my_dog.eat()
my_dog.bark()

